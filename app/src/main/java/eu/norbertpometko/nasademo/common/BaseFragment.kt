package eu.norbertpometko.nasademo.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import eu.norbertpometko.nasademo.BR
import eu.norbertpometko.nasademo.di.Injectable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment <T: ViewDataBinding, V: BaseViewModel> (@LayoutRes open val layoutResId: Int) : Fragment(), Injectable {

    protected abstract val viewModel: V

    private lateinit var binding: T

    private val disposables = CompositeDisposable()

    private var navigationDisposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
        binding.setVariable(BR.viewModel, viewModel)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigationDisposable?: run{ navigationDisposable = viewModel.navigationPublisher.subscribe { onNavigationEvent(it) } }
    }

    protected fun onNavigationEvent(navDirections: NavDirections) {
        findNavController().navigate(navDirections)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
        disposables.dispose()
        disposables.clear()
    }

    protected fun Disposable.disposeOnDestroy() {
        disposables.add(this)
    }
}

package eu.norbertpometko.nasademo.common.bindingadapters

import android.widget.TextView
import androidx.databinding.BindingAdapter

@Suppress("UNCHECKED_CAST")
@BindingAdapter("bind:int")
fun TextView.bindInt(value: Int?) {
    value ?: return
    text = value.toString()
}

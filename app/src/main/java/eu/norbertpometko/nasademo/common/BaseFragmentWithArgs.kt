package eu.norbertpometko.nasademo.common

import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.navigation.NavArgs

abstract class BaseFragmentWithNavArgs<T : ViewDataBinding, V : BaseViewModelWithNavArgs<A>, A: NavArgs>
    (@LayoutRes override val layoutResId: Int) : BaseFragment<T, V>(layoutResId)

package eu.norbertpometko.nasademo.common.bindingadapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import eu.norbertpometko.nasademo.common.BindableRecyclerViewAdapter

@Suppress("UNCHECKED_CAST")
@BindingAdapter("bind:items")
fun <ITEM> RecyclerView.bindItems(items: List<ITEM>?) {
    items ?: return
    if (adapter is BindableRecyclerViewAdapter<*>) {
        (adapter as BindableRecyclerViewAdapter<ITEM>).items = items
    }
}

package eu.norbertpometko.nasademo.common

import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

abstract class BaseViewModel : ViewModel() {

    private val disposables = CompositeDisposable()

    val navigationPublisher = PublishSubject.create<NavDirections>()
    
    abstract fun start()

    fun onDestroy() {
        disposables.dispose()
        disposables.clear()
    }

    protected fun Disposable.autoDispose() {
        disposables.add(this)
    }
}

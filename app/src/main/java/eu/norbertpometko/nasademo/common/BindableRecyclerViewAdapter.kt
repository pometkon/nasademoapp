package eu.norbertpometko.nasademo.common

import androidx.recyclerview.widget.RecyclerView

//TODO: use generalized ViewHolder with databinding
abstract class BindableRecyclerViewAdapter<ITEM>:  RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items: List<ITEM> = listOf()
    set(value) {
        field = value
        notifyDataSetChanged()
    }
}

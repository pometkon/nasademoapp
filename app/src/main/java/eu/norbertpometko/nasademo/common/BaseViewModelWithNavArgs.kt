package eu.norbertpometko.nasademo.common

import androidx.navigation.NavArgs

abstract class BaseViewModelWithNavArgs<A : NavArgs> : BaseViewModel() {

    abstract fun start(navArgs: A)

    override fun start() {
        //Implement it here empty so that it doesn't clutter the VM
    }
}

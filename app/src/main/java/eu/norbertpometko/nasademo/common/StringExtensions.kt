package eu.norbertpometko.nasademo.common

import android.content.Context

fun String.Companion.resolveStringResByName(context: Context, resourceName: String): String {
    val packageName = context.packageName
    val resId = context.resources.getIdentifier(resourceName, "string", packageName)
    if (resId == 0)
        throw IllegalStateException("Could not resolve resource by name: $resourceName")
    return context.getString(resId)
}

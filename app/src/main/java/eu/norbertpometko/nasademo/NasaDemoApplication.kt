package eu.norbertpometko.nasademo

import android.app.Activity
import android.app.Application
import dagger.android.*
import eu.norbertpometko.nasademo.di.DaggerApplicationComponent
import javax.inject.Inject
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import eu.norbertpometko.nasademo.di.AppInjector

class NasaDemoApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        AppInjector.init(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector
}

package eu.norbertpometko.nasademo.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import eu.norbertpometko.nasademo.screens.imageList.ImageListFragment
import eu.norbertpometko.nasademo.screens.roverdetails.RoverDetailsFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeRoverDetailsFragment(): RoverDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeImageListFragment(): ImageListFragment
}

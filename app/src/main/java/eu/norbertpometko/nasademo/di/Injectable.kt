package eu.norbertpometko.nasademo.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable

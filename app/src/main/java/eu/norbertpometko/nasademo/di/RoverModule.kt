package eu.norbertpometko.nasademo.di

import dagger.Module
import dagger.Provides
import eu.norbertpometko.nasademo.R
import eu.norbertpometko.nasademo.screens.roverdetails.RoverItemModel
import eu.norbertpometko.nasaroverapi.models.CameraType
import eu.norbertpometko.nasaroverapi.models.RoverName

/* No rover list and basiic rover info is provided by the API, so we need to declare it somewhere */
@Module
object RoverModule {
    @JvmStatic
    @Provides
    fun provideRoverInfo(): List<RoverItemModel> =
        listOf(
            RoverItemModel(
                RoverName.CURIOSITY,
                R.drawable.curiosity,
                listOf(
                    CameraType.FHAZ,
                    CameraType.RHAZ,
                    CameraType.MAST,
                    CameraType.CHEMCAM,
                    CameraType.MAHLI,
                    CameraType.MARDI,
                    CameraType.NAVCAM
                )
            ),
            RoverItemModel(
                RoverName.OPPORTUNITY,
                R.drawable.opportunity,
                listOf(
                    CameraType.FHAZ,
                    CameraType.RHAZ,
                    CameraType.NAVCAM,
                    CameraType.PANCAM,
                    CameraType.MINITES
                )
            ),
            RoverItemModel(
                RoverName.SPIRIT,
                R.drawable.opportunity,
                listOf(
                    CameraType.FHAZ,
                    CameraType.RHAZ,
                    CameraType.NAVCAM,
                    CameraType.PANCAM,
                    CameraType.MINITES
                )
            )
        )
}

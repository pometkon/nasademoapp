package eu.norbertpometko.nasademo.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import eu.norbertpometko.nasademo.NasaDemoApplication
import eu.norbertpometko.nasaroverapi.di.NasaRoverApiModule
import eu.norbertpometko.network.di.NetworkModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        MainActivityModule::class,
        NetworkModule::class,
        NasaRoverApiModule::class,
        ViewModelModule::class,
        RoverModule::class
    ]
)

interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(githubApp: NasaDemoApplication)
}

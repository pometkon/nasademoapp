package eu.norbertpometko.nasademo.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import eu.norbertpometko.nasademo.screens.imageList.ImageListViewModel
import eu.norbertpometko.nasademo.screens.roverdetails.RoverDetailsScreenViewModel

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ImageListViewModel::class)
    abstract fun bindImageListViewModel(viewModel: ImageListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RoverDetailsScreenViewModel::class)
    abstract fun bindUserViewModel(userScreenViewModel: RoverDetailsScreenViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}

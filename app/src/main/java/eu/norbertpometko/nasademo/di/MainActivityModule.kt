package eu.norbertpometko.nasademo.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import eu.norbertpometko.nasademo.screens.main.MainActivity

@Suppress("unused")
@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity
}

package eu.norbertpometko.nasademo.screens.imageList

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import eu.norbertpometko.nasademo.common.BaseFragmentWithNavArgs
import eu.norbertpometko.nasademo.R
import eu.norbertpometko.nasademo.databinding.FragmentImageListBinding
import kotlinx.android.synthetic.main.fragment_image_list.*
import javax.inject.Inject

class ImageListFragment : BaseFragmentWithNavArgs<FragmentImageListBinding, ImageListViewModel, ImageListFragmentArgs>(
    R.layout.fragment_image_list
) {

    private val args: ImageListFragmentArgs by navArgs()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: ImageListViewModel
            by lazy { ViewModelProviders.of(this, viewModelFactory).get(ImageListViewModel::class.java) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imagesRecyclerView.adapter = ImageRecyclerViewAdapter {
            //TODO
        }
        imagesRecyclerView.layoutManager = LinearLayoutManager(context)

        viewModel.start(args)
    }
}

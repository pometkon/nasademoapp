package eu.norbertpometko.nasademo.screens.roverdetails

import androidx.annotation.DrawableRes
import eu.norbertpometko.nasaroverapi.models.CameraType
import eu.norbertpometko.nasaroverapi.models.RoverName

data class RoverItemModel(val name: RoverName, @DrawableRes val imageResId: Int, val cameras: List<CameraType>)

package eu.norbertpometko.nasademo.screens.imageList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import eu.norbertpometko.nasademo.common.BindableRecyclerViewAdapter
import eu.norbertpometko.nasademo.R
import eu.norbertpometko.nasaroverapi.models.Photo

class ImageRecyclerViewAdapter(private val onItemClick: (Photo) -> Unit) :
    BindableRecyclerViewAdapter<Photo>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder =
        ImageViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
        )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val imageViewHolder = holder as ImageViewHolder
        val item = items[position]
        Glide.with(imageViewHolder.imageView.context).load(item.imgSrc).into(imageViewHolder.imageView)
        imageViewHolder.imageView.setOnClickListener { onItemClick(item) }
    }
}

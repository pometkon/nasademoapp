package eu.norbertpometko.nasademo.screens.roverdetails

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import eu.norbertpometko.nasademo.common.BaseViewModel
import eu.norbertpometko.nasaroverapi.models.PhotoManifestRoot
import eu.norbertpometko.nasaroverapi.network.NasaRoverApiService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RoverDetailsScreenViewModel @Inject constructor(
    private val api: NasaRoverApiService,
    val roverItemModels: List<RoverItemModel>
) : BaseViewModel() {

    private var manifestRequestDisposable: Disposable? = null

    val isLoading = ObservableBoolean(true)

    val roverInfoViewModels = ObservableField<List<RoverInfoUiModel>>()

    val selectedRover = ObservableField<RoverInfoUiModel>()

    override fun start() {
        isLoading.set(true)
        val roverInfoViewModels = mutableListOf<RoverInfoUiModel>()
        val roverManifestQueryObservables: List<Single<PhotoManifestRoot>> = roverItemModels.map {
            api.getManifest(it.name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
        manifestRequestDisposable = Single.zip(roverManifestQueryObservables) { items -> items }
            .subscribe ({ results ->
                results.forEach {
                    val photoManifestGeneric = (it as PhotoManifestRoot).photoManifestGeneric
                    val roverItemModel = roverItemModels.findByName(photoManifestGeneric.name)

                    roverInfoViewModels.add(
                        RoverInfoUiModel.fromPhotoManifestGeneric(roverItemModel!!, photoManifestGeneric)
                    )
                }
                this.roverInfoViewModels.set(roverInfoViewModels)
                selectedRover.set(roverInfoViewModels[0])

                isLoading.set(false)
            }, {
                navigationPublisher.onNext(RoverDetailsFragmentDirections.actionRoverDetailsFragmentToErrorFragment(it.localizedMessage))
            })
    }

    fun selectRover(position: Int) {
        val roverInfoViewModel = roverInfoViewModels.get()?.get(position)
        selectedRover.set(roverInfoViewModel)
    }
}

fun List<RoverItemModel>.findByName(name: String?): RoverItemModel? {
    val normalizedName = name?.toUpperCase()
    return this.find { roverItemModel -> roverItemModel.name.toString() == normalizedName }
}

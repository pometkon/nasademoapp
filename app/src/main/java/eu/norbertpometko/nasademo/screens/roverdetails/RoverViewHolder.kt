package eu.norbertpometko.nasademo.screens.roverdetails

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_rover.view.*

class RoverViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val imageView: ImageView = view.roverImage
    val nameTextView: TextView = view.roverName
}

package eu.norbertpometko.nasademo.screens.roverdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import eu.norbertpometko.nasademo.common.BindableRecyclerViewAdapter
import eu.norbertpometko.nasademo.R
import eu.norbertpometko.nasademo.common.resolveStringResByName
import eu.norbertpometko.nasaroverapi.models.CameraType

class CameraRecyclerViewAdapter(private val onItemClick: (CameraType) -> Unit) :
    BindableRecyclerViewAdapter<CameraType>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CameraViewHolder =
        CameraViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_camera, parent, false)
        )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val cameraViewHolder = holder as CameraViewHolder
        cameraViewHolder.nameTextView.text =
            String.resolveStringResByName(holder.nameTextView.context, "camera_${items[position]}")
        cameraViewHolder.root.setOnClickListener { onItemClick(items[position]) }
    }
}

package eu.norbertpometko.nasademo.screens.roverdetails

import androidx.annotation.DrawableRes
import eu.norbertpometko.nasaroverapi.models.CameraType
import eu.norbertpometko.nasaroverapi.models.PhotoManifestGeneric

data class RoverInfoUiModel(
    val name: String,
    val status: String,
    val landingDate: String,
    val launchDate: String,
    val photoCount: Int,
    val maxSol: Int,
    val cameras: List<CameraType>,
    @DrawableRes var imageResId: Int
) {
    companion object {
        fun fromPhotoManifestGeneric(rover: RoverItemModel, photoManifestGeneric: PhotoManifestGeneric) =
            RoverInfoUiModel(
                photoManifestGeneric.name,
                photoManifestGeneric.status,
                photoManifestGeneric.landingDate,
                photoManifestGeneric.launchDate,
                photoManifestGeneric.totalPhotos,
                photoManifestGeneric.maxSol,
                rover.cameras,
                rover.imageResId
            )
    }
}

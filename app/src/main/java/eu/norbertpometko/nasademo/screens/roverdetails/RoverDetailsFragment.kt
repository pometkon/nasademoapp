package eu.norbertpometko.nasademo.screens.roverdetails

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.norbertpometko.nasademo.common.BaseFragment
import eu.norbertpometko.nasademo.R
import eu.norbertpometko.nasademo.databinding.FragmentRoverDetailsBinding
import eu.norbertpometko.nasademo.view.common.SnapOnScrollListener
import eu.norbertpometko.nasademo.common.createSnapperHelper
import kotlinx.android.synthetic.main.fragment_rover_details.*
import javax.inject.Inject

class RoverDetailsFragment : BaseFragment<FragmentRoverDetailsBinding, RoverDetailsScreenViewModel>(
        R.layout.fragment_rover_details
    ) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: RoverDetailsScreenViewModel
            by lazy { ViewModelProviders.of(this, viewModelFactory).get(RoverDetailsScreenViewModel::class.java) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.start()

        setupRoverRecyclerView()

        adjustChevronVisibility(0, viewModel.roverInfoViewModels.get()?.size ?: 1)

        setupCameraRecyclerView()
    }

    private fun setupRoverRecyclerView() {
        roverRecyclerView.adapter = RoverRecyclerViewAdapter()
        roverRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        roverRecyclerView.layoutAnimation = AnimationUtils.loadLayoutAnimation(
            context,
            R.anim.layout_animation_fall_down
        )

        val snapHelper = createSnapperHelper()
        snapHelper.attachToRecyclerView(roverRecyclerView)

        val snapOnScrollListener = SnapOnScrollListener(
            snapHelper,
            SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL_STATE_IDLE
        ) { position ->
            adjustChevronVisibility(position, viewModel.roverInfoViewModels.get()?.size ?: 1)
            viewModel.selectRover(position)
        }
        roverRecyclerView.addOnScrollListener(snapOnScrollListener)

        roverRecyclerView.scheduleLayoutAnimation()
    }

    private fun setupCameraRecyclerView() {
        cameraRecyclerView.adapter = CameraRecyclerViewAdapter {
            val roverName = viewModel.roverItemModels.findByName(viewModel.selectedRover.get()?.name)?.name
            viewModel.navigationPublisher.onNext(
                RoverDetailsFragmentDirections.actionRoverDetailsFragmentToImageListFragment(
                    roverName!!, it, viewModel.selectedRover.get()!!.maxSol)
            )
        }
        cameraRecyclerView.layoutManager = LinearLayoutManager(context)
        cameraRecyclerView.overScrollMode = View.OVER_SCROLL_NEVER
        cameraRecyclerView.scheduleLayoutAnimation()
    }

    private fun adjustChevronVisibility( //TODO: handle this with binding
        position: Int,
        listItemCount: Int
    ) {
        leftChevron.visibility =
            if (position == 0)
                View.GONE
            else
                View.VISIBLE

        rightChevron.visibility =
            if (position == listItemCount - 1)
                View.GONE
            else
                View.VISIBLE
    }
}

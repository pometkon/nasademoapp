package eu.norbertpometko.nasademo.screens.roverdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import eu.norbertpometko.nasademo.common.BindableRecyclerViewAdapter
import eu.norbertpometko.nasademo.R

class RoverRecyclerViewAdapter : BindableRecyclerViewAdapter<RoverInfoUiModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoverViewHolder =
        RoverViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_rover, parent, false)
        )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val roverViewHolder = holder as RoverViewHolder
        roverViewHolder.nameTextView.text = items[position].name
        roverViewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(holder.imageView.context, items[position].imageResId))
    }
}

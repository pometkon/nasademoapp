package eu.norbertpometko.nasademo.screens.imageList

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import eu.norbertpometko.nasademo.common.BaseViewModelWithNavArgs
import eu.norbertpometko.nasaroverapi.models.Photo
import eu.norbertpometko.nasaroverapi.network.NasaRoverApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ImageListViewModel @Inject constructor(
    private val api: NasaRoverApiService
) : BaseViewModelWithNavArgs<ImageListFragmentArgs>() {

    val isLoading = ObservableBoolean(true)

    val photos = ObservableField<List<Photo>>()

    override fun start(navArgs: ImageListFragmentArgs) {
        isLoading.set(true)

        api.getPhotos(navArgs.roverName, navArgs.sol, navArgs.cameraType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { error -> navigationPublisher.onNext(
                ImageListFragmentDirections.actionImageListFragmentToErrorFragment(error.localizedMessage))
            }
            .subscribe { result ->
                photos.set(result.photos)
                isLoading.set(false)
            }
            .autoDispose()
    }
}

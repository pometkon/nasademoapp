package eu.norbertpometko.nasademo.screens.roverdetails

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_camera.view.*

class CameraViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val root: ViewGroup = view.root
    val nameTextView: TextView = view.cameraName
}

package eu.norbertpometko.nasademo.screens.error

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import eu.norbertpometko.nasademo.R
import kotlinx.android.synthetic.main.fragment_error.*

class ErrorFragment : Fragment(R.layout.fragment_error) {
    private val args: ErrorFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        errorDetailsTextView.text = args.error
    }
}

package eu.norbertpometko.nasaroverapi.di

import dagger.Module
import dagger.Provides
import eu.norbertpometko.nasaroverapi.network.NasaRoverApiService
import retrofit2.Retrofit

@Module
class NasaRoverApiModule {

    @Provides
    fun provideNasaRoverApiService(retrofit: Retrofit) : NasaRoverApiService =
        retrofit.create(NasaRoverApiService::class.java)

}

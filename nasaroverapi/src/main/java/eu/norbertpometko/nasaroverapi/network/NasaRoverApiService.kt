package eu.norbertpometko.nasaroverapi.network

import eu.norbertpometko.nasaroverapi.models.CameraType
import eu.norbertpometko.nasaroverapi.models.PhotoManifestRoot
import eu.norbertpometko.nasaroverapi.models.Photos
import eu.norbertpometko.nasaroverapi.models.RoverName
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NasaRoverApiService {

    @GET("mars-photos/api/v1/rovers/{rover}/latest_photos")
    fun getLatestPhotos(@Path("rover") roverName: RoverName): Single<Photos>

    @GET("mars-photos/api/v1/rovers/{rover}/photos?")
    fun getPhotos(@Path("rover") roverName: RoverName, @Query("sol") sol: Int): Single<Photos>

    @GET("mars-photos/api/v1/rovers/{rover}/photos?")
    fun getPhotos(@Path("rover") roverName: RoverName, @Query("sol") sol: Int, @Query("camera") camera: CameraType): Single<Photos>

    @GET("mars-photos/api/v1/rovers/{rover}/photos?")
    fun getPhotos(@Path("rover") roverName: RoverName, @Query("earth_date") date: String): Single<Photos>

    @GET("mars-photos/api/v1/rovers/{rover}/photos?")
    fun getPhotos(@Path("rover") roverName: RoverName, @Query("earth_date") date: String, @Query("camera") camera: CameraType): Single<Photos>

    @GET("mars-photos/api/v1/manifests/{rover}?")
    fun getManifest(@Path("rover") roverName: RoverName): Single<PhotoManifestRoot>
}

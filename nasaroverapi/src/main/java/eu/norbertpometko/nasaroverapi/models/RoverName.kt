package eu.norbertpometko.nasaroverapi.models

enum class RoverName {
    CURIOSITY,
    OPPORTUNITY,
    SPIRIT
}

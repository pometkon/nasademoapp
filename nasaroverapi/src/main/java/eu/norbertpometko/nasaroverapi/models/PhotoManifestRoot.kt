package eu.norbertpometko.nasaroverapi.models

import com.google.gson.annotations.SerializedName

data class PhotoManifestRoot(
    @SerializedName("photo_manifest")
    val photoManifestGeneric: PhotoManifestGeneric
)

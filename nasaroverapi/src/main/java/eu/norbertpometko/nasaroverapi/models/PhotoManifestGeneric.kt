package eu.norbertpometko.nasaroverapi.models

import com.google.gson.annotations.SerializedName

data class PhotoManifestGeneric(
    @SerializedName("landing_date")
    val landingDate: String,
    @SerializedName("launch_date")
    val launchDate: String,
    @SerializedName("max_date")
    val maxDate: String,
    @SerializedName("max_sol")
    val maxSol: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("photos")
    val photos: List<PhotoManifest>,
    @SerializedName("status")
    val status: String,
    @SerializedName("total_photos")
    val totalPhotos: Int
)

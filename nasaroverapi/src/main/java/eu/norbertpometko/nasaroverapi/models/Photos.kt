package eu.norbertpometko.nasaroverapi.models

import com.google.gson.annotations.SerializedName

data class Photos(
    @SerializedName("photos")
    val photos: List<Photo>
)

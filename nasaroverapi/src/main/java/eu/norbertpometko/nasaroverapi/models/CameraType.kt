package eu.norbertpometko.nasaroverapi.models

enum class CameraType {
    FHAZ,
    RHAZ,
    MAST,
    CHEMCAM,
    MAHLI,
    MARDI,
    NAVCAM,
    PANCAM,
    MINITES
}

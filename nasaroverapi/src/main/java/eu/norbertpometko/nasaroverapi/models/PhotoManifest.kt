package eu.norbertpometko.nasaroverapi.models

import com.google.gson.annotations.SerializedName

data class PhotoManifest(
    @SerializedName("cameras")
    val cameras: List<String>,
    @SerializedName("earth_date")
    val earthDate: String,
    @SerializedName("sol")
    val sol: Int,
    @SerializedName("total_photos")
    val totalPhotos: Int
)
